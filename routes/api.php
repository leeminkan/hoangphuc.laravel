<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'auth'], function(){
    Route::post('signin','AuthController@signIn');
    Route::post('signup','AuthController@signUp');
    Route::post('change-password', 'AuthController@changePassword')->middleware('check.token');
    Route::group(['prefix' => 'password'], function () {    
        Route::post('request-reset-password', 'AuthController@requestResetPassword');
        Route::get('reset-password/{token}', 'AuthController@getResetPasswordInfo');
        Route::post('reset-password', 'AuthController@resetPassword');
    });
});


Route::group(['prefix'=>'user'], function(){
    Route::middleware(['check.token'])->group(function () {
        Route::get('profile', 'UserController@getProfile');
        Route::put('update-profile', 'UserController@updateProfile');
        Route::post('request-add-friend/{friendId}', 'UserController@requestAddFriend');
        Route::get('request-add-friend', 'UserController@getListFriendRequest');
        Route::post('accept-request-friend/{friendId}', 'UserController@AcceptRequestFriend');
        Route::get('list-friend', 'UserController@getListFriend');
    });
});

Route::group(['prefix'=>'post'], function(){
    Route::middleware(['check.token'])->group(function () {
        Route::post('create', 'UserPostController@createPost');
        Route::get('read/{postId}', 'UserPostController@readPost');
        Route::get('new-feed', 'UserPostController@getNewFeed');
    });
});
//route test for backend developer
Route::get('test', 'UserController@test');