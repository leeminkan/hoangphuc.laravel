<?php
/**
 * 
 * @SWG\Tag(
 *   name="Users",
 *   description="Operations about user",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about our store",
 *     url="http://swagger.io"
 *   )
 * ),
 * @SWG\Tag(
 *   name="Auth",
 *   description="Operations about user",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about our store",
 *     url="http://swagger.io"
 *   )
 * ),
 * @SWG\Tag(
 *   name="Relationship",
 *   description="Relationship of user",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about our store",
 *     url="http://swagger.io"
 *   )
 * ),
 * @SWG\Tag(
 *   name="Posts",
 *   description="Relationship of user",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about our store",
 *     url="http://swagger.io"
 *   )
 * ),
 * @SWG\Tag(
 *   name="Test",
 *   description="Test",
 *   @SWG\ExternalDocumentation(
 *     description="Find out more about our store",
 *     url="http://swagger.io"
 *   )
 * )
 */