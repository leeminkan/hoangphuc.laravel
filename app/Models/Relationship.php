<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Relationship extends BaseModel
{
    //
    protected $table = 'relationship';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_one_id', 'user_two_id', 'status', 'action_user_id'
    ];
    public static $Status = array(
        '1' => 'Requested',
        '2' => 'Accepted',
        '3' => 'Declined',
        '4' => 'Blocked',
    );
    public function GetUserOne()
    {
    	return User::find($this->user_one_id);
    }
    public function GetUserTwo()
    {
    	return User::find($this->user_two_id);
    }
    public function GetUserAction()
    {
    	return User::find($this->action_user_id);
    }
    public function GetOtherIdUser()
    {
        if($this->action_user_id != $this->user_one_id)
        {
            return $this->user_one_id;
        }
    	return $this->user_two_id;
    }
    public function GetOtherUser()
    {
    	return User::find($this->GetOtherIdUser());
    }
    public function GetAnotherId($id)
    {
        if($id != $this->user_one_id)
        {
            return $this->user_one_id;
        }
    	return $this->user_two_id;
    }
    public function GetAnotherUser($id)
    {
        return User::find($this->GetAnotherId($id));
    }
    public function GetStatus()
    {
        return self::$Status[$this->status];
    }



    public static function CheckFriend($idUser1, $idUser2)
    {
        $relationship = Relationship::where([
            ['user_one_id', $idUser1],
            ['user_two_id', $idUser2],
        ])
        ->orwhere([
            ['user_one_id', $idUser2],
            ['user_two_id', $idUser1],
        ])->first();
        if ($relationship['status'] == 2) return true;
        return false;
    }

    //static function
    public static function GetStatusGeneral($code)
    {
        return self::$Status[$code];
    }
    //Get record
    public static function GetListRequestFriendGeneral($idUser)
    {  
        $relationships = Relationship::where(function($q) use($idUser) {
            $q->where('user_one_id', $idUser)
              ->orWhere('user_two_id', $idUser);
        })
        ->where([
            ['status', '=', '1'],
            ['action_user_id', '<>', 5],
        ])->get();
        return $relationships;
    }
    //Get list user
    public static function GetListRequestFriendUser($idUser)
    {   
        $relationships = Relationship::GetListRequestFriendGeneral($idUser);
        //Array of object Users
        $listFriendRequest = array();
        foreach ($relationships as $relationship) {
            $listFriendRequest[] = $relationship->GetUserAction();
        }
        return $listFriendRequest;
    }
    //Get record
    public static function GetListFriendGeneral($idUser)
    {   
        $relationships = Relationship::where(function($q) use($idUser) {
            $q->where('user_one_id', $idUser)
              ->orWhere('user_two_id', $idUser);
        })
        ->where('status', '=', '2')->get();
        return $relationships;
    }
    //Get list id user
    public static function GetListFriendId($idUser)
    {   
        $relationships = Relationship::GetListFriendGeneral($idUser);
        //Array of object Users
        $listFriendId = array();
        foreach ($relationships as $relationship) {
            $listFriendId[] = $relationship->GetAnotherId($idUser);
        }
        return $listFriendId;
    }
    //Get list user
    public static function GetListFriendUser($idUser)
    {   
        $relationships = Relationship::GetListFriendGeneral($idUser);
        //Array of object Users
        $listFriend = array();
        foreach ($relationships as $relationship) {
            $listFriend[] = $relationship->GetAnotherUser($idUser);
        }
        return $listFriend;
    }
}
