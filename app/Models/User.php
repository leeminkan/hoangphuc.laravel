<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;


class User extends BaseModel implements Authenticatable
{
    use HasApiTokens, Notifiable, AuthenticableTrait;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static $rules = array(
        'Rule_Create_User' => [
            'name' => 'required',
            'email' => 'required_without:phone|unique:users,email|regex:/^[a-z][a-z0-9_\.]{2,}@[a-z0-9]{2,}(\.[a-z0-9]{2,}){1,2}$/',
            'phone' => 'required_without:email|unique:users,phone|regex:/^[0-9]{1,3} [0-9]{9,}$/',
            'password' => 'required|string|min:6|max:16',
            'repassword' => 'required|same:password',
            'date_of_birth' => 'required|date|date_format:Y-m-d',
            'gender' => 'required|in:male,female',
            'description' => 'required|max:100'
        ],
        'Rule_Signin' => [
            'email' => 'required_without:phone|regex:/^[a-z][a-z0-9_\.]{2,}@[a-z0-9]{2,}(\.[a-z0-9]{2,}){1,2}$/',
            'phone' => 'required_without:email|regex:/^[0-9]{1,3} [0-9]{9,}$/',
            'password' => 'required|string'
        ],
        'Rule_ChangPassword' => [
            'password' => 'required|string',
            'newpassword' => 'required|string|min:6|max:16',
            'repassword' => 'required|same:newpassword'
        ],
        'Rule_RequestResetPassword' => [
            'email' => 'required|regex:/^[a-z][a-z0-9_\.]{2,}@[a-z0-9]{2,}(\.[a-z0-9]{2,}){1,2}$/'
        ],
        'Rule_GetResetPasswordInfo' => [
            'token' => 'required|string'
        ],
        'Rule_ResetPassword' => [
            'token' => 'required|string',
            'password' => 'required|string',
            'repassword' => 'required|same:password'
        ],
        'Rule_UpdateProfile' => [
            'name' => 'required',
            'date_of_birth' => 'required|date|date_format:Y-m-d',
            'gender' => 'required|in:male,female',
            'description' => 'required|max:100'
        ]
    );


}
