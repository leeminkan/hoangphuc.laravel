<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class UserPost extends BaseModel
{
    //
    protected $table = 'user_post';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId', 'content', 'share'
    ];
    public static $ShareConvert = array(
        '1' => 'OnlyMe',
        '2' => 'Friend',
        '3' => 'EveryOne',
    );
    public static $rules = array(
        'Rule_Create_Post' => [
            'content' => 'required|string',
            'share' => 'required||regex:/^[1-3]{1}$/',
        ],
        'Rule_Read_Post' => [
            'postId' => 'required|integer|exists:user_post,id',
        ],
    );
    //Get record
    public static function GetNewFeed($listFriend = array(),$numberPost = 5)
    {
        //Array of object UserPost
        $newFeed = array();
        foreach ($listFriend as $idFriend) {
            $posts = UserPost::where('userId',$idFriend)->get();
            foreach ($posts as $post) {
                $newFeed[] = $post;
            }
        }
        $newFeed = collect($newFeed)->sortByDesc('updated_at')->take($numberPost);
        return $newFeed;
    }
}