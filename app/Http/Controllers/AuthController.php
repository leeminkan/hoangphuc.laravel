<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Validator;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use App\User;

class AuthController extends BaseApiController
{
    /**
     * @SWG\POST(
     *      path="/auth/signin",
     *      operationId="signinUser",
     *      tags={"Auth"},
     *      summary="Logs user into the system",
     *      description="Logs user into the system",
     *      @SWG\Parameter(
     *      name="body",
     *      description="Created user object",
     *      required=true,
     *      in="body",
     *      @SWG\Schema(
     *              @SWG\Property(
     *                  property="emailOrPhone",
     *                  type="string",
     *                  default="kan@gmail.com"
     *              ),
     *              @SWG\property(
     *                  property="password",
     *                  type="string",
     *                  default="14121412"
     *              )
     *          ),
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function signIn(Request $request)
    {
        try
        {
            //Check mail or phone
            $field = filter_var($request->emailOrPhone, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
            $request->merge([$field => $request->emailOrPhone]);
            //Validate
            $input = $request->all();
            $validator = User::validate($input, 'Rule_Signin');
            if ($validator) {
                //return
                return $this->responseErrorValidator($validator, 422);
            }
            //Signin authentication
            if($request->email)
            {
                $user = User::where('email', $request->email)->first();
                if(!$user)
                {
                    //return
                    return $this->responseErrorCustom('user_not_found', 404);
                }
                $credentials = $request->only('email', 'password');
            }
            else
            {
                $user = User::where('phone', $request->phone)->first();
                if(!$user)
                {
                    //return
                    return $this->responseErrorCustom('user_not_found', 404);
                }
                $credentials = $request->only('phone', 'password');
            }
            if(!Auth::attempt($credentials))
            {
                //return
                return $this->responseErrorCustom('user_password_invalid', 400);
            } 
            else{
                $user = Auth::user();
                $result['data'] = ['token' => $user->createToken('Kan')->accessToken];
                //return
                return $this->responseSuccess($result);
            }
        }
        catch (\Exception $exception) 
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }
    /**
     * @SWG\POST(
     *      path="/auth/signup",
     *      operationId="signupUser",
     *      tags={"Auth"},
     *      summary="Register user",
     *      description="Register user",
     *      @SWG\Parameter(
     *      name="body",
     *      description="Created user object",
     *      required=true,
     *      in="body",
     *      @SWG\Schema(
     *              @SWG\Property(
     *                  property="name",
     *                  type="string",
     *                  default="kan"
     *              ),
     *              @SWG\Property(
     *                  property="emailOrPhone",
     *                  type="string",
     *                  default="kan@gmail.com"
     *              ),
     *              @SWG\property(
     *                  property="password",
     *                  type="string",
     *                  default="14121412"
     *              ),
     *              @SWG\property(
     *                  property="repassword",
     *                  type="string",
     *                  default="14121412"
     *              ),
     *              @SWG\property(
     *                  property="date_of_birth",
     *                  type="string",
     *                  format="date"
     *              ),
     *              @SWG\property(
     *                  property="gender",
     *                  type="string",
     *                  default="male"
     *              ),
     *              @SWG\property(
     *                  property="description",
     *                  type="string",
     *                  default="About Kan"
     *              )
     *          ),
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function signUp(Request $request)
    {
        try
        {
            //Check mail or phone
            $field = filter_var($request->emailOrPhone, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
            $request->merge([$field => $request->emailOrPhone]);
            //Validate
            $input = $request->all();
            $validator = User::validate($input, 'Rule_Create_User');
            if ($validator) {
                //return
                return $this->responseErrorValidator($validator, 422);
            }
            //Create User
            $user = new User;
            $user->name = $request->name;
            if($request->email)
            {
                $user->email = $request->email;
            }
            else
            {
                $user->phone = $request->phone;
            }
            $user->date_of_birth = date('Y/m/d', strtotime($request->date_of_birth));
            $user->gender = $request->gender;
            $user->description = $request->description;
            $user->password = bcrypt($request->password);
            $user->save();
            //create token to send user
            $data['name'] =  $user->name;
            $data['token'] =  $user->createToken('Kan')->accessToken;
            $result['data'] = $data;
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception) 
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }

    /**
     * @SWG\POST(
     *      path="/auth/change-password",
     *      operationId="ChangePassword",
     *      tags={"Auth"},
     *      summary="Change Password User",
     *      description="Change Password User",
     *      security={
     *       {"passport": {"*"}},
     *      },
     *      @SWG\Parameter(
     *      name="body",
     *      description="Created user object",
     *      required=true,
     *      in="body",
     *      @SWG\Schema(
     *              @SWG\Property(
     *                  property="password",
     *                  type="string",
     *                  default="14121412"
     *              ),
     *              @SWG\property(
     *                  property="newpassword",
     *                  type="string",
     *                  default="14121412"
     *              ),
     *              @SWG\property(
     *                  property="repassword",
     *                  type="string",
     *                  default="14121412"
     *              )
     *          ),
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function changePassword(Request $request)
    {
        try
        {
            //Validate
            $input = $request->all();
            $validator = User::validate($input, 'Rule_ChangPassword');
            if ($validator) {
                //return
                return $this->responseErrorValidator($validator, 422);
            }
            //Check password
            $user = User::find($request->oauth_user_id);
            if (!(Hash::check($request->password, $user->password)))
            {
                //return
                return $this->responseErrorCustom('user_password_invalid', 400);
            }
            //Change password
            User::where('email', $user->email)->update([
                'password' => bcrypt($request->newpassword)
            ]);
            $data = ['message' => 'Change password successfull!!'];
            $result['data'] = $data;
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception) 
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }
    /**
     * @SWG\POST(
     *      path="/auth/password/request-reset-password",
     *      tags={"Auth"},
     *      description="Request token to reset password",
     *      @SWG\Parameter(
     *      name="body",
     *      description="Provide email to get token",
     *      required=true,
     *      in="body",
     *      @SWG\Schema(
     *              @SWG\Property(
     *                  property="email",
     *                  type="string",
     *                  default="kan@gmail.com"
     *              )
     *          ),
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function requestResetPassword(Request $request)
    {
        try
        {
            //Validate
            $input = $request->all();
            $validator = User::validate($input, 'Rule_RequestResetPassword');
            if ($validator) {
                //return
                return $this->responseErrorValidator($validator, 422);
            }

            //If cant find user
            $user = User::where('email', $request->email)->first();
            if (!$user)
            {
                //return
                return $this->responseErrorCustom('user_not_found', 404);
            }
            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                [
                    'email' => $user->email,
                    'token' => str_random(60),
                    'expires_at' => Carbon::now()->addMinutes(1)
                ]
            );
            if ($passwordReset)
            {
                $user->notify(
                    new PasswordResetRequest($passwordReset->token)
                );
            }
            $data['name'] =  $user->name;
            $data['message'] =  'We have e-mailed your password reset link!';
            $data['token'] =  $passwordReset->token;
            $result['data'] = $data;
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception) 
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }   
    }

    /**
     * @SWG\GET(
     *      path="/auth/password/reset-password/{token}",
     *      tags={"Auth"},
     *      description="Get info with token, check token",
     *      @SWG\Parameter(
     *          name="token",
     *          description="Check token reset password, get info reset password",
     *          required=true,
     *          type="string",
     *          in="path"
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function getResetPasswordInfo($token)
    {
        try
        {
            //Validate
            $input = ['token'=>$token];
            $validator = User::validate($input, 'Rule_GetResetPasswordInfo');
            if ($validator) {
                //return
                return $this->responseErrorValidator($validator, 422);
            }
            //Get info with Token
            $passwordReset = PasswordReset::where('token', $token)->first();
            if (!$passwordReset)
            {
                //return
                return $this->responseErrorCustom('token_not_found', 404);
            }
            if (Carbon::parse($passwordReset->expires_at)->isPast()) {
                //return
                return $this->responseErrorCustom('token_expired', 404);
            }
            $result['data'] = $passwordReset;
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception) 
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }   
        
    }
    /**
     * @SWG\POST(
     *      path="/auth/password/reset-password",
     *      tags={"Auth"},
     *      description="Reset password",
     *      @SWG\Parameter(
     *      name="body",
     *      description="Provide email to get token",
     *      required=true,
     *      in="body",
     *      @SWG\Schema(
     *              @SWG\Property(
     *                  property="token",
     *                  type="string",
     *                  default="   "
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  type="string",
     *                  default="password"
     *              ),
     *              @SWG\Property(
     *                  property="repassword",
     *                  type="string",
     *                  default="repassword"
     *              )
     *          ),
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function resetPassword(Request $request, $token)
    {
        try
        {
            //Validate
            $input = $request->all();
            $validator = User::validate($input, 'Rule_ResetPassword');
            if ($validator) {
                //return
                return $this->responseErrorValidator($validator, 422);
            }
            //Check token reset password
            $passwordReset = PasswordReset::where([
                ['token', $token]
            ])->first();
            if (!$passwordReset)
            {
               //return
               return $this->responseErrorCustom('token_not_found', 404);
            }
            if (Carbon::parse($passwordReset->expires_at)->isPast()) {
                //return
                return $this->responseErrorCustom('token_expired', 404);
            }
            //Check user
            $user = User::where('email', $passwordReset->email)->first();
            if (!$user)
            {
                //return
                return $this->responseErrorCustom('user_not_found', 404);
            }
            //Reset Password
            $user->password = bcrypt($request->password);
            $user->save();
            //delete token
            $passwordReset->delete();
            //send mail to notify
            $user->notify(new PasswordResetSuccess($passwordReset));
            $result['data'] = $user;
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception) 
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }
}
