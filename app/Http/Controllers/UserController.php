<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use DB;

use App\Relationship;
use App\User;

class UserController extends BaseApiController
{

    /**
     * @SWG\GET(
     *      path="/user/profile",
     *      operationId="getProfile",
     *      tags={"Users"},
     *      security={
     *       {"passport": {"*"}},
     *      },
     *      summary="Profile User",
     *      description="Profile User",
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function getProfile(Request $request)
    {
        try
        {
            $user = User::find($request->oauth_user_id);
            $result['data'] = $user;
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception) 
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }
    
    /**
     * @SWG\PUT(
     *      path="/user/update-profile",
     *      operationId="UpdateProfile",
     *      tags={"Users"},
     *      summary="Update profile User",
     *      security={
     *       {"passport": {"*"}},
     *      },
     *      description="Update profile User",
     *      @SWG\Parameter(
     *      name="body",
     *      description="Created user object",
     *      required=true,
     *      in="body",
     *      @SWG\Schema(
     *              @SWG\Property(
     *                  property="name",
     *                  type="string",
     *                  default="kan"
     *              ),
     *              @SWG\property(
     *                  property="date_of_birth",
     *                  type="string",
     *                  format="date"
     *              ),
     *              @SWG\property(
     *                  property="gender",
     *                  type="string",
     *                  default="male",
     *                  description="male or female"
     *              ),
     *              @SWG\property(
     *                  property="description",
     *                  type="string",
     *                  default="About Kan"
     *              )
     *          ),
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function updateProfile(Request $request)
    {
        try
        {
            //Validate
            $input = $request->all();
            $validator = User::validate($input, 'Rule_UpdateProfile');
            if ($validator) {
                //return
                return $this->responseErrorValidator($validator, 422);
            }
            User::where('id', $request->oauth_user_id)->update([
                'name' => $request->name,
                'date_of_birth' => date('Y/m/d', strtotime($request->date_of_birth)),
                'gender' => $request->gender,
                'description' => $request->description,
            ]);
            $data = ['message' => 'Update profile successfull!!'];
            $result['data'] = $data;
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception) 
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
        
    }
    /**
     * @SWG\POST(
     *      path="/user/request-add-friend/{friendId}",
     *      tags={"Relationship"},
     *      operationId="UserAddFriend",
     *      summary="User Add Friend",
     *      description="User Add Friend",
     *      security={
     *       {"passport": {"*"}},
     *      },
     *      @SWG\Parameter(
     *          name="friendId",
     *          description="fiendId is id of user who you want to make friend with",
     *          required=true,
     *          type="string",
     *          in="path"
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function requestAddFriend(Request $request, $friendId)
    {
        try
        {
            $statusCode = 1;
            $action_user_id = $request->oauth_user_id;
            if ($action_user_id == $friendId)
            {
                //return
                return $this->responseErrorCustom('something_wrong', 500);
            }
            if ($action_user_id < $friendId)
            {
                $input = [
                    'user_one_id' => $request->oauth_user_id,
                    'user_two_id' => $friendId,
                ];
            }
            else
            {
                $input = [
                    'user_one_id' => $friendId, 
                    'user_two_id' => $request->oauth_user_id,
                ];
            }
            $rules = [
                'user_one_id' =>
                'required|exists:users,id|unique:relationship,user_one_id,NULL,id,user_two_id,'.$input['user_two_id'],
                'user_two_id' => 
                'required|exists:users,id',
            ];
            //Validate
            $validator = User::validateCustomRule($input, $rules);
            if ($validator) {
                //return
                return $this->responseErrorValidator($validator, 422);
            }
            $relationship = Relationship::Create(
                ['user_one_id' => $input['user_one_id'], 'user_two_id' => $input['user_two_id'], 'status' => $statusCode, 'action_user_id' => $action_user_id]
            );
            $result['message'] = $relationship->GetStatus();
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception)
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }
    /**
     * @SWG\GET(
     *      path="/user/request-add-friend",
     *      tags={"Relationship"},
     *      operationId="GetListFriendRequest",
     *      summary="Get List Friend Request",
     *      description="Get List Friend Request",
     *      security={
     *       {"passport": {"*"}},
     *      },
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     **/
    public function getListFriendRequest(Request $request)
    {
        try
        {
            $idUser = $request->oauth_user_id;
            $listFriendRequest = Relationship::GetListRequestFriendUser($idUser);
            return $this->responseSuccess($listFriendRequest);
        }
        catch (\Exception $exception)
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }
    /**
     * @SWG\POST(
     *      path="/user/accept-request-friend/{friendId}",
     *      tags={"Relationship"},
     *      operationId="UserAcceptFriend",
     *      summary="User Accept Friend",
     *      description="User Accept Friend",
     *      security={
     *       {"passport": {"*"}},
     *      },
     *      @SWG\Parameter(
     *          name="friendId",
     *          description="fiendId is id of user who you want to accept friend",
     *          required=true,
     *          type="string",
     *          in="path"
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function AcceptRequestFriend(Request $request, $friendId)
    {
        try
        {
            $statusCode = 2;
            $old_action_user_id = $friendId;
            $action_user_id = $request->oauth_user_id;
            if ($action_user_id == $friendId)
            {
                //return
                return $this->responseErrorCustom('something_wrong', 500);
            }
            if ($action_user_id < $friendId)
            {
                $input = [
                    'user_one_id' => $request->oauth_user_id,
                    'user_two_id' => $friendId,
                ];
            }
            else
            {
                $input = [
                    'user_one_id' => $friendId, 
                    'user_two_id' => $request->oauth_user_id,
                ];
            }
            $rules = [
                'user_one_id' =>'required|exists:users,id',
                'user_two_id' =>'required|exists:users,id',
            ];
            //Validate
            $validator = User::validateCustomRule($input, $rules);
            if ($validator) {
                //return
                return $this->responseErrorValidator($validator, 422);
            }
            $relationship = Relationship::where('user_one_id', $input['user_one_id'])
            ->where('user_two_id', $input['user_two_id'])->first();
            if(!$relationship)
            {
                //return
                return $this->responseErrorCustom('something_wrong', 500);
            }
            if($relationship->value('status') != 1)
            {
                //return
                return $this->responseErrorCustom('something_wrong', 500);
            }
            if($relationship->value('action_user_id') == $old_action_user_id)
            {
                //return
                return $this->responseErrorCustom('something_wrong', 500);
            }
            $relationship->update([
                'status' => $statusCode,
                'action_user_id' => $action_user_id
            ]);
            $result['message'] = $relationship->GetStatus();
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception)
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }
    /**
     * @SWG\GET(
     *      path="/user/list-friend",
     *      tags={"Relationship"},
     *      operationId="GetListFriend",
     *      summary="Get List Friend",
     *      description="Get List Friend",
     *      security={
     *       {"passport": {"*"}},
     *      },
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     **/
    public function getListFriend(Request $request)
    {
        try
        {
            $idUser = $request->oauth_user_id;
            $listFriend = Relationship::GetListFriendUser($idUser);
            return $this->responseSuccess($listFriend);
        }
        catch (\Exception $exception)
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }
    /**
     * @SWG\GET(
     *      path="/test",
     *      tags={"Test"},
     *      operationId="Test",
     *      summary="Test",
     *      description="Test",
     *      security={
     *       {"passport": {"*"}},
     *      },
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     **/
    public function test(Request $request)
    {
        $relationships = Relationship::GetListRequestFriendGeneral(5);
        //Array of object Users
        $list = array();
        foreach ($relationships as $relationship) {
            $list[] = $relationship->GetUserAction();
        }
        return $this->responseSuccess($list);
    }
}
