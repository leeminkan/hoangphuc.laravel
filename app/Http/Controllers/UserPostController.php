<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\UserPost;
use App\Relationship;

class UserPostController extends BaseApiController
{
    /**
     * @SWG\POST(
     *      path="/post/create",
     *      operationId="createPost",
     *      tags={"Posts"},
     *      summary="Create Post",
     *      description="Create Post",
     *      security={
     *       {"passport": {"*"}},
     *      },
     *      @SWG\Parameter(
     *      name="body",
     *      description="Created post",
     *      required=true,
     *      in="body",
     *      @SWG\Schema(
     *              @SWG\property(
     *                  property="content",
     *                  type="string",
     *                  default="About Kan"
     *              ),
     *              @SWG\property(
     *                  property="share",
     *                  type="integer",
     *                  default="About Kan"
     *              ),
     *          ),
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function createPost(Request $request)
    {
    	try
        {
            //Validate
            $input = $request->all();
            $validator = UserPost::validate($input, 'Rule_Create_Post');
            if ($validator) {
                //return
                return $this->responseErrorValidator($validator, 422);
            }
            $post = UserPost::create([
                'userId' => $request->oauth_user_id,
                'content' => $request->content,
                'share' => $request->share,
            ]);
            $data = [
                'message' => 'Create post successfull!!',
                'post' => $post,
            ];
            $result['data'] = $data;
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception) 
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }
    /**
     * @SWG\GET(
     *      path="/post/read/{postId}",
     *      tags={"Posts"},
     *      description="description",
     *      security={
     *       {"passport": {"*"}},
     *      },
     *      @SWG\Parameter(
     *          name="postId",
     *          description="description",
     *          required=true,
     *          type="string",
     *          in="path"
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function readPost(Request $request, $postId)
    {
    	try
        {
            //Validate
            $input = [
                'postId' => $postId
            ];
            $validator = UserPost::validate($input, 'Rule_Read_Post');
            if ($validator) {
                //return
                return $this->responseErrorValidator($validator, 422);
            }
            $post = UserPost::find($postId);
            if ($request->oauth_user_id != $post->userId)
            {
                $checkFiend = Relationship::CheckFriend($post->userId,$request->oauth_user_id);
                if (!$checkFiend)
                {
                    //return
                    return $this->responseErrorCustom('post_not_found', 404);
                }
            }
            
            $result['data'] = $post;
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception) 
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }
    /**
     * @SWG\GET(
     *      path="/post/new-feed",
     *      operationId="getNewFeed",
     *      tags={"Posts"},
     *      security={
     *       {"passport": {"*"}},
     *      },
     *      summary="Get New Feed",
     *      description="Get New Feed",
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Invalid request params"),
     *      @SWG\Response(response=401, description="Request is not authenticated"),
     *      @SWG\Response(response=404, description="Not Found"),
     *     )
     *
     */
    public function getNewFeed(Request $request)
    {
    	try
        {
            $listFriend = Relationship::GetListFriendId(5);
            $listFriend[] = 5;
            $newFeed = UserPost::GetNewFeed($listFriend);
            $result['data'] = $newFeed;
            //return
            return $this->responseSuccess($result);
        }
        catch (\Exception $exception) 
        {
            //return
            return $this->responseErrorException($exception->getMessage(),99999, 500);
        }
    }
}