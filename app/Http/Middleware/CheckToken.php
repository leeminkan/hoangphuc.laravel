<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Lang;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\ValidationData;
use League\OAuth2\Server\CryptKey;
use RuntimeException;
use App\User;
use DB;
use App\Http\Controllers\BaseApiController as BaseApiController;

class CheckToken extends BaseApiController
{
    /**
     * Another: https://github.com/laravel/passport/issues/71
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next)
    {
        //get JWT
        $jwt = $request->bearerToken();
        //Check Example:
        //incorrect: authorization: "Bxxx xxxxxx", authorizationsss: "Bearer xxxxxx"
        //correct: authorization: "Bearer xxxxxx"
        if (!$jwt)
        {
            //return
            return $this->responseErrorCustom('unauthorized', 401);
        }
        //Set key
        $file = "oauth-public.key";
        $pathkey = storage_path($file);
        $oauth = [];
        try {
            // Attempt to parse and validate the JWT
            //Check Example:
            //incorrect: "Bearer xxxszagdg.fgfaxxx", "Bearer xxxxxx"
            //correct: authorization: "Bearer aa.bb.cccccc" with it can parse to json 
            $token = (new Parser())->parse($jwt);
            try {
                // Verify Token with public key
                if ($token->verify(new Sha256(), 'file://' . $pathkey)  === false) {
                    //return
                    return $this->responseErrorCustom('unauthorized', 401);
                }
            } catch (BadMethodCallException $exception) {
                //return
                return $this->responseErrorCustom('unauthorized', 401);
            }
            // Ensure access token hasn't expired
            $data = new ValidationData();
            $data->setCurrentTime(time());
            if ($token->validate($data) === false) {
                //return
                return $this->responseErrorCustom('token_expired', 401);
            }
            //get info from token
            $oauth['oauth_access_token_id'] = $token->getClaim('jti');
            $oauth['oauth_client_id'] = $token->getClaim('aud');
            $oauth['oauth_user_id'] = $token->getClaim('sub');
            $oauth['oauth_scopes'] = $token->getClaim('scopes');
        } catch (\InvalidArgumentException $exception) {
            // JWT couldn't be parsed so return the request as is
            //return
            return $this->responseErrorCustom('token_invalid', 401);
        } catch (\RuntimeException $exception) {
            //JWR couldn't be parsed so return the request as is
            //return
            return $this->responseErrorCustom('token_invalid', 401);
        }
        //find user with oauth_user_id
        if (!User::find($oauth['oauth_user_id']))
        {
            //return
            return $this->responseErrorCustom('user_not_found', 401);
        }
        $findtoken = DB::table('oauth_access_tokens')->where('id', $oauth['oauth_access_token_id'])->first();
        if (!$findtoken)
        {
            //return
            return $this->responseErrorCustom('token_not_found', 401);
        }
        $request->request->add(['oauth_user_id' => $oauth['oauth_user_id']]);
        return $next($request);
    }
}