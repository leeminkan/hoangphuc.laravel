<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
class FixSwagger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if (strpos($response->headers->get("authorization"),"Bearer ") === false) {
            $response->headers->set("authorization","Bearer ".$response->headers->get("authorization"));
        }
        //return response()->json($request->headers->get("authorization"));
        return $response;
    }
}
