<?php
$apiErrorMessages = array(
    //user
    'user_not_found' => 'The user is not found.',
    'user_password_invalid' => 'The password is invalid.',

    'user_name_required' => 'The name field is required.',
    'user_email_required' => 'The email field is required.',
    'user_email_required_without' => 'The mail field is required when phone is not present.',
    'user_email_regex' => 'The email format is invalid.',
    'user_email_unique' => 'The email has already been taken.',
    'user_phone_required_without' => 'The phone field is required when mail is not present.',
    'user_phone_regex' => 'The phone format is invalid.',
    'user_phone_unique' => 'The phone has already been taken.',
    'user_password_required' => 'The password field is required.',
    'user_password_string' => 'The password must be a string.',
    'user_password_min' => 'The password must be at least 6 characters.',
    'user_password_max' => 'The password may not be greater than 16 characters.',
    'user_newpassword_required' => 'The newpassword field is required.',
    'user_newpassword_min' => 'The newpassword must be at least 6 characters.',
    'user_newpassword_max' => 'The newpassword may not be greater than 16 characters.',
    'user_repassword_required' => 'The repassword field is required.',
    'user_repassword_same' => 'The password and repassword must match.',
    'user_dob_required' => 'The date_of_birth field is required.',
    'user_dob_date' => 'The date_of_birth is not a valid date.',
    'user_dob_date_format' => 'The date_of_birth does not match the format:Y-m-d',
    'user_gender_required' => 'The gender field is required.',
    'user_gender_in' => 'The selected gender is invalid.',
    'user_description_required' => 'The description field is required.',
    'user_description_max' => 'The description may not be greater than 100 characters.',

    //token
    'token_required' => 'The token is required.',
    'token_string' => 'The token must be a string.',

    'token_invalid' => 'The token is invalid.',
    'token_not_found' => 'The token is not found.',
    'token_expired' => 'The token is expired.',

    //auth
    'unauthorized' => 'The user is unauthorized.',
    //relationship
    'user_one_id_require' => 'The user one id is required.',
    'user_one_id_unique' => 'The user one id and user two id is unique.',
    'user_two_id_require' => 'The user two id is required.',
    'already_friend' => 'Already friend',
    //post
    'content_require' => 'Content is required',
    'content_string' => 'Content must be string',
    'share_require' => 'The share is required',
    'share_regex' => 'The share must be an integer in 1 - 3',
    'postId_require' => 'PostId is required',
    'postId_integer' => 'PostId must be string',
    'postId_exists' => 'PostId must be exists',
    'post_not_found' => 'PostId not found',
    //Server
    'something_wrong' => 'Something was wrong',
);

$apiErrorCodes = array(
    //user
    'user_not_found' => 1001,
    'user_password_invalid' => 1002,

    'user_name_required' => 1101,
    'user_email_required' => 1111,
    'user_email_required_without' => 1112,
    'user_email_regex' => 1113,
    'user_email_unique' => 1114,
    'user_phone_required_without' => 1122,
    'user_phone_regex' => 1123,
    'user_phone_unique' => 1124,
    'user_password_required' => 1131,
    'user_password_string' => 1132,
    'user_password_min' => 1133,
    'user_password_max' => 1134,
    'user_newpassword_required' => 1141,
    'user_newpassword_min' => 1142,
    'user_newpassword_max' => 1143,
    'user_repassword_required' => 1151,
    'user_repassword_same' => 1152,
    'user_dob_required' => 1161,
    'user_dob_date' => 1162,
    'user_dob_date_format' => 1163,
    'user_gender_required' => 1171,
    'user_gender_in' => 1172,
    'user_description_required' => 1181,
    'user_description_max' => 1182,
    //token
    'token_required' => 2101,
    'token_string' => 2102,
    'token_invalid' => 2103,
    'token_not_found' => 2104,
    'token_expired' => 2105,
    
    //auth
    'unauthorized' => 3101,
    //relationship
    'user_one_id_require' => 4101,
    'user_one_id_unique' => 4102,
    'user_two_id_require' => 4111,
    'already_friend' => 4121,
    'relationship_not_found' => 4901,
    //post
    'content_require' => 5101,
    'content_string' => 5102,
    'share_require' => 5201,
    'share_regex' => 5202,
    'postId_require' => 5301,
    'postId_integer' => 5302,
    'postId_exists' => 5303,
    'post_not_found' => 5404,
    //Server
    'something_wrong' => 5101,
);

$ApiCodes['ApiErrorMessages'] = $apiErrorMessages;
$ApiCodes['ApiErrorCodes'] = $apiErrorCodes;
return $ApiCodes;
