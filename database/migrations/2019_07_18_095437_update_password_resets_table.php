<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePasswordResetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('password_resets', function($table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dateTime('expires_at');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('password_resets', function($table) {
            $table->timestamps();
            $table->dropColumn('expires_at');
         });
    }
}
